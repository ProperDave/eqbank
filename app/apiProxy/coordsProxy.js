import request from 'ajax-request'
import Promise from 'promise'

function getZipCoords(zip){
    return new Promise(function (resolve, reject) {
      request({
        url: window.location.origin + '/api/coord/' + zip,
        method: 'GET'
      }, function(err, res, body) {
        if(err) reject(err);
        resolve(JSON.parse(body));
      });
    });
}

function getZipsInRange(latitude, longitude, radius){
    return new Promise(function (resolve, reject) {
      request({
        url: window.location.origin + '/api/zip/range/' + latitude + '/' + longitude + '/' + radius,
        method: 'GET'
      }, function(err, res, body) {
        if(err) reject(err);
        resolve(JSON.parse(body));
      });
    });
}

export default {
    getZipCoords: getZipCoords,
    getZipsInRange: getZipsInRange
}