import request from 'ajax-request'
import Promise from 'promise'

function get(data){
    return new Promise(function (resolve, reject) {
      request({
        url: window.location.origin + '/api/locations',
        method: 'GET',
        data: data 
      }, function(err, res, body) {
        if(err) reject(err);
        resolve(JSON.parse(body));
      });
    });
}

export default {
    get: get
}