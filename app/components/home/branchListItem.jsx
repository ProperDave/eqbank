import React from 'react';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import Button from '@material-ui/core/Button';
import {Redirect} from 'react-router-dom';

export default class BranchListItem extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            selectedBranch: null  
        };
    }
    
    handleClick = (e) => {
        this.setState({selectedBranch: this.props.UNINUM});
    }
    
    render(){
        if(this.state.selectedBranch != null){
            return (<Redirect to={'/branch/' + this.state.selectedBranch} />);
        }
        
        var fullAddress = this.props.ADDRESS + ', ' + this.props.CITY + ', ' + this.props.STNAME + ' ' + this.props.ZIP

        return (
            <ListItem key={this.props.UNINUM}>
                <ListItemText primary={this.props.NAME}/>
                <ListItemText secondary={fullAddress}/>
                <ListItemSecondaryAction>
                    <Button variant="contained" color="primary" key={this.props.UNINUM} onClick={this.handleClick}>Select</Button>
                </ListItemSecondaryAction>
            </ListItem>
        );
    }
}