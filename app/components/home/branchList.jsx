import React from 'react';
import List from '@material-ui/core/List';
import BranchListItem from './branchListItem';

export default class BranchList extends React.Component{
    constructor(props){
        super(props);
    }
    
    render(){
        return (
            <List>
                {this.props.branches.map((i) => <BranchListItem {...i.data} key={i.data.UNINUM} />)}
            </List>
        );
    }
}