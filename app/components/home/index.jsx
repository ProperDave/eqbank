import React from 'react';
import locationsProxy from '../../apiProxy/locationProxy';
import coordsProxy from '../../apiProxy/coordsProxy';
import BranchList from './branchList';
import {Redirect} from 'react-router-dom';
import { TextField, Button, Select, MenuItem, OutlinedInput } from '@material-ui/core';

export default class Home extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            selectedInstitution: JSON.parse(localStorage.getItem('selectedInstitution')),
            branches: [],
            selectedZip: "",
            selectedDistance: "0",
            isZipErrored: false
        }
    }
    
    componentDidMount(){
        var self = this;
        if(this.state.selectedInstitution != null){
            locationsProxy.get({
                filters: 'FI_UNINUM:' + this.state.selectedInstitution.UNINUM
            }).then(function(result){
                self.setState({branches: result.data});
            });
        }        
    }

    handleClick = (e) => {
        if(this.state.selectedZip.length != 5){
            this.setState({isZipErrored: true});
            return;
        }        
        this.setState({isZipErrored: false});
        var self = this;
        coordsProxy.getZipCoords(this.state.selectedZip).then(function(coords){
            coordsProxy.getZipsInRange(coords.latitude, coords.longitude, self.state.selectedDistance).then(function(zips){
                if(self.state.selectedInstitution != null){
                    locationsProxy.get({
                        filters: 'FI_UNINUM:' + self.state.selectedInstitution.UNINUM + ' AND ZIP:(' + zips.join(' OR ') + ')'
                    }).then(function(result){
                        self.setState({branches: result.data});
                    });
                }
            });
        });             
    }
    
    handleZipChange = (e) => {
        this.setState({selectedZip: e.target.value});
    }
    
    handleDistanceChange = (e) => {
        this.setState({selectedDistance: e.target.value});
    }

    render(){
        if(this.state.selectedInstitution == null){
            return(<Redirect to='/search' />);
        }
        
        return (<div>
            <h1>Welcome to branch selection!</h1>
            <TextField variant="outlined" label="Zip" onChange={this.handleZipChange} error={this.state.isZipErrored} value={this.state.selectedZip}></TextField>
            <Select label="Miles" onChange={this.handleDistanceChange} value={this.state.selectedDistance} input={
                <OutlinedInput/>
            }>
                <MenuItem value="0">None</MenuItem>
                <MenuItem value="5">5</MenuItem>
                <MenuItem value="10">10</MenuItem>
                <MenuItem value="20">20</MenuItem>
            </Select>
            <Button variant="contained" color="primary" onClick={this.handleClick}>Filter</Button>
            <BranchList branches={ this.state.branches }/>
        </div>);
    }
}
