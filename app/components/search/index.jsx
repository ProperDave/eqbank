import React from 'react';
import institutionsProxy from '../../apiProxy/institutionProxy';
import SearchList from './searchList';
import TextField from '@material-ui/core/TextField';
import {Redirect} from 'react-router-dom';

export default class Search extends React.Component{
    constructor(props){
        super(props);

        this.state = {
            selectedInstitution: localStorage.getItem('selectedInstitution'),
            institutions: []
        };
    }

    handleChange = (e) => {
        var self = this;
        if(e.target.value.length >= 3){
            institutionsProxy.get({
                filters: 'NAME:*' + e.target.value + '* AND ACTIVE:1'
            }).then(function(result){
                self.setState({institutions: result.data});
            });
        } else {
            self.setState({institutions: []});
        }
    }

    render(){
        if(this.state.selectedInstitution != null){
            return(<Redirect to='/'/>);
        }

        return (
            <div>
                <h1>Welcome to institution selection!</h1>
                <p>Please enter the name of the Banking Institution you work with:</p>
                <TextField type="text" onChange={this.handleChange} variant='outlined' fullWidth style={{ margin: 8 }}></TextField>
                <SearchList institutions={ this.state.institutions }/>
            </div>
        )
    }
}