import React from 'react';
import List from '@material-ui/core/List';
import SearchListItem from './searchListItem';

export default class SearchList extends React.Component{
    constructor(props){
        super(props);
    }
    
    render(){
        return (
            <List>
                {this.props.institutions.map((i) => <SearchListItem institution={i.data} key={i.data.UNINUM} />)}
            </List>
        );
    }
}