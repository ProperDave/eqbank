import React from 'react';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import Button from '@material-ui/core/Button';
import {Redirect} from 'react-router-dom';

export default class SearchListItem extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            institutionSelected: false  
        };
    }
    
    handleClick = (e) => {
        localStorage.setItem('selectedInstitution', JSON.stringify(this.props.institution));

        this.setState({institutionSelected: true});
    }
    
    render(){
        if(this.state.institutionSelected){
            return (<Redirect to='/' />);
        }
        
        return (
            <ListItem key={this.props.institution.UNINUM}>
                <ListItemText primary={this.props.institution.NAME}/>

                <ListItemSecondaryAction>
                    <Button variant="contained" color="primary" onClick={this.handleClick}>Select</Button>
                </ListItemSecondaryAction>
            </ListItem>
        );
    }
}