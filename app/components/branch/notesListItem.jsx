import React from 'react';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';

export default class NotesListItem extends React.Component{
    constructor(props){
        super(props);
    }
    
    render(){        
        return (
            <ListItem>
                <ListItemText primary={this.props.subject}/>
                <ListItemText secondary={this.props.body}/>
            </ListItem>
        );
    }
}