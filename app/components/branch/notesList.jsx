import React from 'react';
import List from '@material-ui/core/List';
import NotesListItem from './notesListItem';

export default class NotesList extends React.Component{
    constructor(props){
        super(props);
    }
    
    render(){
        return (
            <List>
                {this.props.notes.map((note, index) => <NotesListItem {...note} key={index} />)}
            </List>
        );
    }
}