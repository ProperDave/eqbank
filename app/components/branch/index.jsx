import React from 'react';
import locationsProxy from '../../apiProxy/locationProxy';
import {Link} from 'react-router-dom';
import ArrowBack from '@material-ui/icons/ArrowBack';
import NotesList from './notesList';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';

export default class Branch extends React.Component{
    constructor(props){
        super(props);

        this.state = {
            branch: { NAME: '' },
            notes: JSON.parse(localStorage.getItem('BranchNotes' + this.props.match.params.id)),
            note: { subject:'', body:'' },
            isSubjectErrored: false,
            isBodyErrored: false
        }
    }

    componentDidMount(){
        var self = this;
        locationsProxy.get({
            filters: 'UNINUM:' + this.props.match.params.id
        }).then(function(result){
            self.setState({branch: result.data[0].data});
        });
    }

    handleSubmit = (e) =>{        
        var note = this.state.note;
        var valid = true;
        if(note.body.trim() == ''){    
            valid = false        
            this.setState({isBodyErrored: true});
        } else {
            this.setState({isBodyErrored: false});
        }
        if(valid){
            var notes = this.state.notes;
            if(notes == null)notes = [];
            if(note.subject.trim() == '')note.subject = '(no subject)'
            notes.push(note);
            localStorage.setItem('BranchNotes' + this.props.match.params.id, JSON.stringify(notes));
            this.setState({notes: notes, note: { subject:'', body:'' }});
        }
    }
    
    handleSubjectChange = (e) =>{
        var note = this.state.note;
        this.setState({note: {subject: e.target.value, body:note.body}});
    }
    
    handleBodyChange = (e) =>{        
        var note = this.state.note;
        this.setState({note: {subject: note.subject, body:e.target.value}});
    }

    render(){
        var notes = this.state.notes;
        if(notes == null)notes = [];
        
        var fullAddress = this.state.branch.ADDRESS + ', ' + this.state.branch.CITY + ', ' + this.state.branch.STNAME + ' ' + this.state.branch.ZIP

        return(
            <div>
                <Link to="/"><ArrowBack/>Return to branch selection</Link>
                <p>You are on the branch information screen of {this.state.branch.NAME}</p>
                <p>At: {fullAddress}</p>
                <p>Please add notes where necessary</p>
                <NotesList notes={notes}/>
                <TextField type="text" label="Subject" onChange={this.handleSubjectChange} variant='outlined' value={this.state.note.subject} fullWidth style={{ margin: 8 }}></TextField>
                <TextField type="text" error={this.state.isBodyErrored} multiline label="Note" onChange={this.handleBodyChange} variant='outlined' value={this.state.note.body} fullWidth style={{ margin: 8 }}></TextField>
                <Button variant="contained" color="primary" onClick={this.handleSubmit}>Submit</Button>
            </div>
        );
    }
}