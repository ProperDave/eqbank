import React from 'react';
import {BrowserRouter as Router, Route, Switch, Redirect} from 'react-router-dom'
import Home from './components/home'
import Search from './components/search'
import Branch from './components/branch'

export default class App extends React.Component{
    render(){
        return(
            <Router>
                <Switch>
                    <Route path="/branch/:id" component={Branch}/>
                    <Route path="/search" component={Search}/>
                    <Route path="/" component={Home}/>
                </Switch>
            </Router>
        )
    }
}