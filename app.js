'use strict';

var express = require('express');
var app = module.exports = express();
var server = require('http').Server(app);
var routes = require('./server/routes');
var viewEngine = require('./server/view-engine');

app.set('port', process.env.PORT || 8000);

//  Connect all routes to the application
app.use('/', routes);
app.set('view engine', 'html');

app.use(express.static(__dirname + '/dist'))

app.engine('html', viewEngine.setup(__dirname, app));

server.listen(app.get('port'), () => {
    console.log('App listening on port %d', app.get('port').toString());
});