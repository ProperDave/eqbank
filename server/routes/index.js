var router = require('express').Router();
var apiController = require('../controllers/api');
var dataController = require('../controllers/data');

router.get('/api/institutions', (req, res) => {apiController(req, res)});
router.get('/api/locations', (req, res) => {apiController(req, res)});
router.get('/api/summary', (req, res) => {apiController(req, res)});
router.get('/api/failures', (req, res) => {apiController(req, res)});
router.get('/api/coord/:zip', (req, res) => {dataController.getZipCoordinates(req, res)});
router.get('/api/zip/range/:lat/:lon/:rad', (req, res) => {dataController.getZipsInRange(req, res)});

router.get('/', (req, res) => {
  res.render('index');
});

router.get('/search', (req, res) => {
  res.render('index');
});

router.get('/branch/:id', (req, res) => {
  res.render('index');
});

module.exports = router;