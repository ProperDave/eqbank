var request = require('request');

module.exports = function(req, res) {
    request.get({ url: 'https://banks.data.fdic.gov' + req.url}, function(error, response, body){
        if(!error && response.statusCode == 200){
            res.json(JSON.parse(body));
        }
    });
};