var sqlite3 = require('sqlite3').verbose();

function getZipsInRange(req, res){
    var latitude = parseFloat(req.params.lat);
    var longitude = parseFloat(req.params.lon);
    var radius = req.params.rad;

    var latitudeCoordRange = radius/69.172;
    var longitudeCoordRange = radius/(Math.cos(Math.abs(latitude))*69.172);

    var db = getDatabase();

    var sql = `SELECT zip, 
                latitude, 
                longitude
                FROM zipcodegps2
                WHERE latitude*1.0 BETWEEN $latitudeMin AND $latitudeMax
                AND longitude*1.0 BETWEEN $longitudeMin AND $longitudeMax`

    var params = {
        $latitudeMin: latitude - latitudeCoordRange,
        $latitudeMax: latitude + latitudeCoordRange,
        $longitudeMin: longitude - longitudeCoordRange,
        $longitudeMax: longitude + longitudeCoordRange,
    }

    db.all(sql, params, function(err, rows){
        if(err){
            throw err;
        }
        res.json(rows.map(function(r){return r.zip;}));
    });
}

function getZipCoordinates(req, res){
    var db = getDatabase();

    var sql = `SELECT zip, 
                latitude, 
                longitude
                FROM zipcodegps2
                WHERE zip = ?`

    db.get(sql, [req.params.zip], function(err, row){
        if(err){
            throw err;
        }
        res.json({
            latitude: row.latitude,
            longitude: row.longitude
        });
    });
}

function getDatabase(){
    return new sqlite3.Database(__dirname + "/../data/data.sqlite", sqlite3.OPEN_READONLY, (err) =>{
        if(err){
            throw err;
        }
        console.log('Connected to the zipcoords database.');
    })
}

module.exports = {
    getZipsInRange: getZipsInRange,
    getZipCoordinates: getZipCoordinates
};