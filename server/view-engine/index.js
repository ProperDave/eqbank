var path    = require('path');
var exphbs  = require('express-handlebars');

module.exports = {
    setup: function(dir, app) {
        var views = path.resolve(dir, './server/views');
        var hbs = exphbs.create({
            compiled:      {},
            precompiled:   {},
            extname:       '.html'
        });

        app.set('views', views);
        app.set('view engine', 'html');

        return hbs.engine;
    }
};