var gulp = require('gulp');
var gutil = require("gulp-util");
var uglify = require("gulp-uglify");
var concat = require("gulp-concat");
var cssnano = require("gulp-cssnano");
var clean = require('gulp-clean');
var babelify = require('babelify');
var browserify = require('browserify');
var buffer = require('vinyl-buffer');
var source = require('vinyl-source-stream');

var paths = {
    scripts: ['./src/*.js', './app/*.js'],
    styles: ['./resource/*.css'],
    resource: ['./resource/*.ico']
};

gulp.task('build-scripts', function(){
    gutil.log("Building scripts...")
    var options = {
        entries: "./app/index.jsx",   // Entry point
        extensions: [".js", ".jsx"],            // consider files with these extensions as modules 
        paths: paths.scripts          // This allows relative imports in require, with './scripts/' as root
    };

    return browserify(options)
        .transform(babelify)
        .bundle()
        .pipe(source("site.js"))
        .pipe(buffer())    // Stream files
        .pipe(uglify())
        .pipe(gulp.dest("./dist/script/"));
});

gulp.task('build-styles', function(){
    gutil.log("Building styles...")
    return gulp.src(paths.styles)
        .on('end', function(){ gutil.log("Creating central .css file...")})
        .pipe(concat('site.css'))
        .on('end', function(){ gutil.log("Minifying styles...")})
        .pipe(cssnano())
        .on('end', function(){ gutil.log("Copying to dist...")})
        .pipe(gulp.dest('./dist/style/'))
        .on('end', function(){ gutil.log("Style build complete!")});
});

gulp.task('build-resources', function(){
    gutil.log("Building resources...")
    return gulp.src(paths.resource)
        .on('end', function(){ gutil.log("Copying to dist...")})
        .pipe(gulp.dest('./dist/resource/'))
        .on('end', function(){ gutil.log("Resource build complete!")});
});

gulp.task('build', gulp.parallel('build-scripts', 'build-styles', 'build-resources'));

gulp.task('clean', function(){
    return gulp.src('dist', {read: false})
    .pipe(clean());
});